- cd api
- docker compose up -d
- cd ../cli

Command cli

- go run root.go

Available Commands:

  auth        A brief description of your command
  completion  Generate the autocompletion script for the specified shell
  groups      Groups is a palette of commands to manage groups
  help        Help about any command
  roles       A brief description of your command
  users       A brief description of your command

Command auth

Available Commands:
  create      Create a new user
  login       A brief description of your command
  logout      Logout from the system
  refresh     A brief description of your command

Command Groups

Available Commands:
  create      Create a group
  delete      Delete a group
  list        List groups
  update      Update a group

 Command roles

 Available Commands:
  create      Create a role
  delete      Delete a role
  list        List roles
  update      Update a role

  Command users

  Available Commands:
  delete      Delete a user
  get         Get user by ID
  list        Manage users
  update      Update a user