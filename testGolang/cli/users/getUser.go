/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package users

import (
	"fmt"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// GetUserCmd represents the GetUser command

var getUserCmd = &cobra.Command{
	Use:   "get [id]",
	Short: "Get user by ID",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]
		uri := fmt.Sprintf("api/users/%s", id)
		authToken :=  viper.GetViper().GetString("jwt")
		body, err := utils.MakeRequest("GET",uri, authToken, nil)
		if err != nil {
			return err
		}
		fmt.Println(body)
		return nil
	},
}

func init() {
	UsersCmd.AddCommand(getUserCmd)

}
