/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package users

import (
	"fmt"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// updateUserCmd represents the updateUser command

var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update a user",
	RunE: func(cmd *cobra.Command, args []string) error {

		email, err := cmd.Flags().GetString("email")
		if err != nil {
			return err
		}

		password, err := cmd.Flags().GetString("password")
		if err != nil {
			return err
		}

		id, err := cmd.Flags().GetString("id")
		if err != nil {
			return err
		}
		uri := "/users/" + id
		authToken :=  viper.GetViper().GetString("jwt")

		body := "{\"Email\":\"" + email + "\",\"Password\":\"" + password + "\"}}"
		response, err := utils.MakeRequest("POST", uri, authToken, []byte(body))
		if err != nil {
			return err
		}

		fmt.Println(response)
		return nil
	},
}
func init() {
	UsersCmd.AddCommand(updateCmd)

}
