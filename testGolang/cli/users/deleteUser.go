/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package users

import (
	"fmt"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// deleteUserCmd represents the deleteUser command
var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete a user",
	RunE: func(cmd *cobra.Command, args []string) error {

		id, err := cmd.Flags().GetString("id")
		if err != nil {
			return err
		}

authToken :=  viper.GetViper().GetString("jwt")
		url := "/users/" + id
		body, err := utils.MakeRequest("DELETE", url, authToken, nil)
		if err != nil {
			return err
		}

		fmt.Println(body)
		return nil
	},
}


func init() {
	deleteCmd.Flags().StringP("id", "i", "", "User ID")

	if err := deleteCmd.MarkFlagRequired("id"); err != nil {
		fmt.Println(err)
	}
	
	UsersCmd.AddCommand(deleteCmd)

}
