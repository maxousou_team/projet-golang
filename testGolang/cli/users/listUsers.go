/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package users

import (
	"fmt"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// ListUsersCmd represents the ListUsers command
var listUsersCmd = &cobra.Command{
	Use:   "list",
	Short: "Manage users",
	RunE: func(cmd *cobra.Command, args []string) error {
		uri := "api/users"
authToken :=  viper.GetViper().GetString("jwt")		
		body, err := utils.MakeRequest("GET",uri, authToken, nil)
		if err != nil {
			return err
		}

		fmt.Println(body)
		return nil
	},
}
func init() {
	UsersCmd.AddCommand(listUsersCmd)

}
