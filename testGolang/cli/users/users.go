/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package users

import (
	"github.com/spf13/cobra"
)

// UsersCmd represents the users command
var UsersCmd = &cobra.Command{
	Use:   "users",
	Short: "A brief description of your command",
	Long: ``,

	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func init() {
}
