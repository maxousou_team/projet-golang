/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/

package roles

import (
	"fmt"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	roleName string
	roleDescription string
)

// createRoleCmd represents the createRole command
var createRoleCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a role",
	RunE: func(cmd *cobra.Command, args []string) error {
		uri := "api/roles"
		authToken :=  viper.GetViper().GetString("jwt")

		roleName, err := cmd.Flags().GetString("roleName")
		if err != nil {
			return err
		}
		

		roleDescription, err := cmd.Flags().GetString("roleDescription")
		if err != nil {
			return err
		}

		body := "{\"Name\":\"" + roleName + "\",\"Description\":\"" + roleDescription + "\"}}"
		
		response, err := utils.MakeRequest("POST", uri, authToken, []byte(body))
		if err != nil {
			return err
		}

		fmt.Println(response)
		return nil
	},
}
func init() {

	createRoleCmd.Flags().StringVarP(&roleName ,"roleName", "n", "", "Role name")
	createRoleCmd.Flags().StringVarP(&roleDescription, "roleDescription", "d", "", "Role description")

	
	if err := createRoleCmd.MarkFlagRequired("roleName"); err != nil {
		fmt.Println(err)
	}

	if err := createRoleCmd.MarkFlagRequired("roleDescription"); err != nil {
		fmt.Println(err)
	}

	RolesCmd.AddCommand(createRoleCmd)


}
