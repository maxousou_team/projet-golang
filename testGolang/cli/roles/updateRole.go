/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package roles

import (
	"fmt"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// updateRoleCmd represents the updateRole command
var updateRoleCmd = &cobra.Command{
	Use:   "update",
	Short: "Update a role",
	RunE: func(cmd *cobra.Command, args []string) error {
		authToken :=  viper.GetViper().GetString("jwt")
		roleName, err := cmd.Flags().GetString("roleName")
		if err != nil {
			return err
		}
		roleId, err := cmd.Flags().GetString("roleId")
		if err != nil {
			return err
		}
		roleDescription, err := cmd.Flags().GetString("roleDescription")

		if err != nil {
			return err
		}

		uri := fmt.Sprintf("api/roles/%s", roleId)

		body := "{\"Name\":\"" + roleName + "\",\"Description\":\"" + roleDescription + "\"}}"

		response, err := utils.MakeRequest("PUT", uri, authToken, []byte(body))
		if err != nil {
			return err
		}
		fmt.Println(response)
		return nil
	},
}

func init() {
	updateRoleCmd.Flags().StringP("roleName", "n", "", "Role name")
	updateRoleCmd.Flags().StringP("roleId", "i", "", "Role ID")

	if err := updateRoleCmd.MarkFlagRequired("roleId"); err != nil {
		fmt.Println(err)
	}
	
	if err := updateRoleCmd.MarkFlagRequired("roleName"); err != nil {
		fmt.Println(err)
	}
	RolesCmd.AddCommand(updateRoleCmd)

}
