/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package roles

import (
	"fmt"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// deleteRoleCmd represents the deleteRole command
var deleteRoleCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete a role",
	RunE: func(cmd *cobra.Command, args []string) error {
authToken :=  viper.GetViper().GetString("jwt")
		roleId, err := cmd.Flags().GetString("roleId")
		if err != nil {
			return err
		}
		uri := fmt.Sprintf("api/roles/%s", roleId)
		response, err := utils.MakeRequest("DELETE", uri, authToken, nil)
		if err != nil {
			return err
		}
		fmt.Println(response)
		return nil
	},
}
func init() {
	deleteRoleCmd.Flags().StringP("roleId", "r", "", "Role ID")

	if err := deleteRoleCmd.MarkFlagRequired("roleId"); err != nil {
		fmt.Println(err)
	}

	RolesCmd.AddCommand(deleteRoleCmd)

}
