/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package roles

import (
	"fmt"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// geteRoleCmd represents the geteRole command
var getRolesCmd = &cobra.Command{
	Use:   "list",
	Short: "List roles",
	RunE: func(cmd *cobra.Command, args []string) error {
		uri := "api/roles"
		authToken :=  viper.GetViper().GetString("jwt")
		response, err := utils.MakeRequest("GET", uri, authToken, nil)
		if err != nil {
			return err
		}
		fmt.Println(response)
		return nil
	},
}

func init() {
	RolesCmd.AddCommand(getRolesCmd)

}
