/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package auth

import (
	"encoding/json"
	"fmt"
	"log"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// CreateUserCmd represents the CreateUser command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a new user",
	RunE: func(cmd *cobra.Command, args []string) error {

		email, err := cmd.Flags().GetString("email")
		if err != nil {
			return err
		}

		password, err := cmd.Flags().GetString("password")
		if err != nil {
			return err
		}

		if email == "" || password == ""  {
			return fmt.Errorf("email, password are required")
		}

		url := "auth/register"
		body := "{\"Email\":\"" + email + "\",\"Password\":\"" + password + "\"}"
		response, err := utils.MakeRequest("POST",url, "", []byte(body))
		if err != nil {
			return err
		}
		if err != nil {
			return err
		}

var res LoginResponse
		json.Unmarshal([]byte(response), &res)
		
		fmt.Println("token" + res.Token)
		viper.Set("jwt", res.Token)

		// Save the JWT to the configuration file
		if err := viper.WriteConfig(); err != nil {
			if _, ok := err.(viper.ConfigFileNotFoundError); ok {
				log.Println("Config file not found; creating a new one")
				// Config file not found; create a new one
				err = viper.SafeWriteConfig()
				if err != nil {
					log.Fatalf("Error saving configuration: %s", err)
				}
			} else {
				log.Fatalf("Error writing to configuration: %s", err)
			}
		}

		return nil
	},
}

func init() {
	createCmd.Flags().StringP("email", "e", "", "Email of the user")
	createCmd.Flags().StringP("password", "p", "", "Password of the user")

	if err := createCmd.MarkFlagRequired("email"); err != nil {
		fmt.Println(err)
	}
	if err := createCmd.MarkFlagRequired("password"); err != nil {
		fmt.Println(err)
	}



	AuthCmd.AddCommand(createCmd)
	

}
