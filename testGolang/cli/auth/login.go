/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package auth

import (
	"encoding/json"
	"fmt"
	"log"

	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)


type LoginResponse struct {
	Token string `json:"token"`
}



// loginCmd represents the login command
var loginCmd = &cobra.Command{
	Use:   "login",
	Short: "A brief description of your command",
	Long: ``,
	RunE: func(cmd *cobra.Command, args []string) error {

		email, err := cmd.Flags().GetString("email")
		if err != nil {
			return err
		}
		password, err := cmd.Flags().GetString("password")

		if err != nil {
			return err
		}

		uri := "auth/login"
		body := "{\"Email\":\"" + email + "\",\"Password\":\"" + password + "\"}}"
		response, err := utils.MakeRequest("POST", uri, "", []byte(body))
	
		if err != nil {
			return err
		}

		var res LoginResponse
		json.Unmarshal([]byte(response), &res)
		
		fmt.Println("token" + res.Token)
		viper.Set("jwt", res.Token)

		// Save the JWT to the configuration file
		if err := viper.WriteConfig(); err != nil {
				if _, ok := err.(viper.ConfigFileNotFoundError); ok {
						// Config file not found; create a new one
						err = viper.SafeWriteConfig()
						if err != nil {
								log.Fatalf("Error saving configuration: %s", err)
						}
				} else {
						log.Fatalf("Error writing to configuration: %s", err)
				}
		}

		return nil
	},
}

func init() {
	loginCmd.Flags().StringP("email", "e", "", "Email")
	loginCmd.Flags().StringP("password", "p", "", "Password")

	if err := loginCmd.MarkFlagRequired("email"); err != nil {
		fmt.Println(err)
	}

	if err := loginCmd.MarkFlagRequired("password"); err != nil {
		fmt.Println(err)
	}

	AuthCmd.AddCommand(loginCmd)


}
