/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package auth

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// logoutCmd represents the logout command
var logoutCmd = &cobra.Command{
	Use:   "logout",
	Short: "Logout from the system",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		viper.Set("jwt", "")
	},
}

func init() {
	AuthCmd.AddCommand(logoutCmd)

}
