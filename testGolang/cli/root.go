package main

import (
	"fmt"
	"os"
	"sdv/golang/cli/auth"
	"sdv/golang/cli/groups"
	"sdv/golang/cli/roles"
	"sdv/golang/cli/users"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "api",
	Short: "A brief description of your application",
	Long: ``,
    Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
    },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)


	// setting jwt

	rootCmd.AddCommand(groups.GroupsCmd)
	rootCmd.AddCommand(auth.AuthCmd)
	rootCmd.AddCommand(users.UsersCmd)
	rootCmd.AddCommand(roles.RolesCmd)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.api.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".api" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".api")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
			jwtToken := viper.GetString("jwt")
			if jwtToken != "" {
					fmt.Println("JWT token loaded from config")
			}
}

}

func main() {
	Execute()
}
