/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package groups

import (
	"fmt"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// deleteGroupsCmd represents the deleteGroups command
var deleteGroupCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete a group",
	RunE: func(cmd *cobra.Command, args []string) error {
authToken :=  viper.GetViper().GetString("jwt")
		groupId, err := cmd.Flags().GetString("groupId")
		if err != nil {
			return err
		}
		uri := fmt.Sprintf("api/groups/%s", groupId)
		response, err := utils.MakeRequest("DELETE", uri, authToken, nil)
		if err != nil {
			return err
		}
		fmt.Println(response)
		return nil
	},
}

func init() {
	deleteGroupCmd.Flags().StringP("groupId", "g", "", "Group ID")

	if err := deleteGroupCmd.MarkFlagRequired("groupId"); err != nil {
		fmt.Println(err)
	}
	
	GroupsCmd.AddCommand(deleteGroupCmd)

}
