/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package groups

import (
	"fmt"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// updateeGroupsCmd represents the updateeGroups command
var updateGroupCmd = &cobra.Command{
	Use:   "update",
	Short: "Update a group",
	RunE: func(cmd *cobra.Command, args []string) error {
authToken :=  viper.GetViper().GetString("jwt")
		groupName, err := cmd.Flags().GetString("groupName")
		if err != nil {
			return err
		}
		groupId, err := cmd.Flags().GetString("groupId")
		if err != nil {
			return err
		}


		groupDescription, err := cmd.Flags().GetString("groupDescription")
		if err != nil {
			return err
		}
		

		uri := fmt.Sprintf("api/groups/%s", groupId)
		body := "{\"Name\":\"" + groupName + "\",\"Description\":\"" + groupDescription + "\"}}"
		response, err := utils.MakeRequest("PUT", uri, authToken, []byte(body))
		if err != nil {
			return err
		}
		fmt.Println(response)
		return nil
	},
}

func init() {
	updateGroupCmd.Flags().StringP("groupName", "n", "", "Group name")
	updateGroupCmd.Flags().StringP("groupId", "i", "", "Group ID")

	if err := updateGroupCmd.MarkFlagRequired("groupId"); err != nil {
		fmt.Println(err)
	}
	if err := updateGroupCmd.MarkFlagRequired("groupName"); err != nil {
		fmt.Println(err)
	}

	GroupsCmd.AddCommand(updateGroupCmd)


}
