/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package groups

import (
	"github.com/spf13/cobra"
)

// groupsGroupsCmd represents the groups command
var GroupsCmd = &cobra.Command{
	Use:   "groups",
	Short: "Groups is a palette of commands to manage groups",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}


func init() {

}
