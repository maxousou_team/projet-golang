/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package groups

import (
	"fmt"
	"log"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// createGroupsCmd represents the createGroups command
var createGroupCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a group",
	RunE: func(cmd *cobra.Command, args []string) error {
		uri := "api/groups"
		authToken :=  viper.GetViper().GetString("jwt")

		
	log.Println(authToken)

		groupName, err := cmd.Flags().GetString("groupName")
		if err != nil {
			return err
		}
		groupDescription, err := cmd.Flags().GetString("groupDescription")
		if err != nil {
			return err
		}

		body := "{\"Name\":\"" + groupName + "\",\"Description\":\"" + groupDescription + "\"}}"
		response, err := utils.MakeRequest("POST", uri, authToken, []byte(body))
		if err != nil {
			return err
		}
		fmt.Println(response)
		return nil
	},
}


func init() {
	createGroupCmd.Flags().StringP("groupName", "n", "", "Group name")
	createGroupCmd.Flags().StringP("groupDescription", "d", "", "Group description")

	if err := createGroupCmd.MarkFlagRequired("groupName"); err != nil {
		fmt.Println(err)
	}

	if err := createGroupCmd.MarkFlagRequired("groupDescription"); err != nil {
		fmt.Println(err)
	}


	GroupsCmd.AddCommand(createGroupCmd)

}
