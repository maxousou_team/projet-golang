/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package groups

import (
	"fmt"
	"sdv/golang/cli/utils"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// geteGroupsCmd represents the geteGroups command
var getGroupsCmd = &cobra.Command{
	Use:   "list",
	Short: "List groups",
	RunE: func(cmd *cobra.Command, args []string) error {
		uri := "api/groups"
authToken :=  viper.GetViper().GetString("jwt")
		response, err := utils.MakeRequest("GET", uri, authToken, nil)
		if err != nil {
			return err
		}
		fmt.Println(response)
		return nil
	},
}


func init() {	
	GroupsCmd.AddCommand(getGroupsCmd)


}
