package utils

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
)

func MakeRequest(method string, uri string, authToken string, body []byte) (string, error) {
	var url = "http://localhost:1234/"
	uri = url + uri

	req, err := http.NewRequest(method, uri, bytes.NewBuffer(body))
	if err != nil {
		return "", err
	}

	req.Header.Set("Authorization", authToken)
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	if resp.StatusCode >= 400 {
		return "", fmt.Errorf("HTTP error %d: %s", resp.StatusCode, responseBody)
	}

	return string(responseBody), nil
}
