package models

type RefreshToken struct {
	ID         uint `gorm:"primaryKey"`
	token      string
	expires_at string
}