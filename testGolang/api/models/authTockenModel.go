package models

type AuthToken struct {
	ID         uint `gorm:"primaryKey"`
	token      string
	expires_at string
}