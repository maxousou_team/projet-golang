package models

import "gorm.io/gorm"

type Group struct {
	gorm.Model
	Name        string `gorm:"unique"`
	Parent_group_id string
	Child_group_ids string         
}
