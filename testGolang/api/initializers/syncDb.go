package initializers

import "sdv/golang/api/models"

func SyncDb() {

	DB.Db.AutoMigrate(&models.User{}, &models.AuthToken{}, &models.Group{}, &models.RefreshToken{}, &models.Role{})
}

func DropEverything() {
	migrator := DB.Db.Migrator()
	migrator.DropTable(&models.User{}, &models.AuthToken{}, &models.Group{}, &models.RefreshToken{}, &models.Role{})
}