package controllers

import (
	"net/http"
	"sdv/golang/api/initializers"
	"sdv/golang/api/models"

	"github.com/gin-gonic/gin"
)

func GetRoles(context *gin.Context) {
	var roles []models.Role
	record := initializers.DB.Db.Find(&roles)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}
	context.JSON(http.StatusOK, roles)
}

func GetRole(context *gin.Context) {
	var role models.Role
	id := context.Param("id")
	record := initializers.DB.Db.First(&role, id)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}
	context.JSON(http.StatusOK, role)
}

func PostRole(context *gin.Context) {
	var role models.Role

	if err := context.ShouldBindJSON(&role); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Failed to read body"})
		context.Abort()
		return
	}

	record := initializers.DB.Db.Create(&role)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}
	context.JSON(http.StatusCreated, gin.H{"id": role.ID, "name": role.Name})
}

func UpdateRole(context *gin.Context) {
	var role models.Role

	if err := context.ShouldBindJSON(&role); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Failed to read body"})
		context.Abort()
		return
	}

	record := initializers.DB.Db.Save(&role)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}
	context.JSON(http.StatusCreated, gin.H{"id": role.ID, "name": role.Name})
}


func DeleteRole(context *gin.Context) {
    id := context.Param("id")
    // Delete the role
    initializers.DB.Db.Delete(&models.Role{}, id)

    context.JSON(http.StatusOK, gin.H{"message": "Role deleted successfully"})
}