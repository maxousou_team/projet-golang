package controllers

import (
	"net/http"
	"sdv/golang/api/initializers"
	"sdv/golang/api/models"

	"github.com/gin-gonic/gin"
)

func GetGroups(context *gin.Context) {
	var groups []models.Group
	record := initializers.DB.Db.Find(&groups)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}
	context.JSON(http.StatusOK, groups)
}

func GetGroup(context *gin.Context) {
	var group models.Group
	record := initializers.DB.Db.First(&group, context.Param("id"))
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}
	context.JSON(http.StatusOK, group)
}
func PostGroup(context *gin.Context) {
	var group models.Group
	if err := context.ShouldBindJSON(&group); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Failed to read body"})
		context.Abort()
		return
	}
	record := initializers.DB.Db.Create(&group)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}
	context.JSON(http.StatusCreated, gin.H{"id": group.ID, "name": group.Name})
}

func UpdateGroup(context *gin.Context) {
	var group models.Group
	if err := context.ShouldBindJSON(&group); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Failed to read body"})
		context.Abort()
		return
	}
	record := initializers.DB.Db.Save(&group)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}
	context.JSON(http.StatusCreated, gin.H{"id": group.ID, "name": group.Name})
}
func DeleteGroup(context *gin.Context) {
	var group models.Group
	record := initializers.DB.Db.Delete(&group, context.Param("id"))
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}
	context.JSON(http.StatusOK, gin.H{"message": "Group deleted"})
}