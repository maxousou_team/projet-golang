package main

import (
	"sdv/golang/api/controllers"
	"sdv/golang/api/initializers"
	"sdv/golang/api/middlewares"

	"github.com/gin-gonic/gin"
)

func init(){
	initializers.LoadEnvVariables()
	initializers.ConnectToDb()
	initializers.SyncDb()
}

func main() {
	router := gin.Default()

	authRoutes := router.Group("/auth")
	{
		authRoutes.POST("/register", controllers.RegisterUser)
		authRoutes.POST("/login", controllers.GenerateToken)
		authRoutes.POST("/refresh", controllers.RefreshToken)
	}

	// Protected routes
	apiRoutes := router.Group("/api")
	apiRoutes.Use(middlewares.Auth())
	{
		rolesRoutes := apiRoutes.Group("/roles")
		{
			rolesRoutes.GET("", controllers.GetRoles)
			rolesRoutes.POST("", controllers.PostRole)
			rolesRoutes.PUT("/:id", controllers.UpdateRole)
			rolesRoutes.DELETE("/:id", controllers.DeleteRole)
		}

		groupsRoutes := apiRoutes.Group("/groups")
		{
			groupsRoutes.GET("", controllers.GetGroups)
			groupsRoutes.POST("", controllers.PostGroup)
			groupsRoutes.PUT("/:id", controllers.UpdateGroup)
			groupsRoutes.DELETE("/:id", controllers.DeleteGroup)

		}

		usersRoutes := apiRoutes.Group("/users")
		{
			usersRoutes.GET("", controllers.GetUsers)
			usersRoutes.GET("/:id", controllers.GetUser)
			usersRoutes.POST("", controllers.PostUser)
			usersRoutes.PUT("/:id", controllers.UpdateUser)
			usersRoutes.DELETE("/:id", controllers.DeleteUser)
		}
	}

	router.Run()
}

